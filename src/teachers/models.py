import datetime
import random

from core.models import Person

from django.db import models


from groups.models import Group


class Teacher(Person):
    birth_date = models.DateField(null=True, default=datetime.date.today)
    salary = models.PositiveIntegerField(default=42)

    group = models.ForeignKey(
        to=Group,
        null=True,
        on_delete=models.SET_NULL,
        related_name='teachers'
    )

    def __str__(self):
        return f'''{self.first_name}, {self.last_name}, {self.age},
                {self.phone_number}, {self.birth_date}, {self.email}, {self.group}'''

    @classmethod
    def _generate(cls):
        obj = super()._generate()
        obj.salary = random.randint(100, 1000)
        obj.save()
