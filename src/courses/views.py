from courses.forms import CourseBaseForm, CourseFilter, CourseUpdateForm
from courses.models import Course

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import DeleteView, ListView, UpdateView


class CourseListView(ListView):
    model = Course
    template_name = 'courses-list.html'

    def get_filter(self):
        return CourseFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        return context


class CourseUpdateView(LoginRequiredMixin, UpdateView):
    model = Course
    form_class = CourseUpdateForm
    template_name = 'courses-update.html'
    success_url = reverse_lazy('course:list')
    pk_url_kwarg = 'course_id'


class CourseDeleteView(LoginRequiredMixin, DeleteView):
    model = Course
    form_class = CourseBaseForm
    template_name = 'courses-delete.html'
    success_url = reverse_lazy('course:list')
    pk_url_kwarg = 'course_id'
