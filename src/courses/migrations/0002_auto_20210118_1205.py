# Generated by Django 3.1.4 on 2021-01-18 12:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('teachers', '0007_auto_20210118_1047'),
        ('groups', '0003_group_headman'),
        ('courses', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='group',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='courses', to='groups.group'),
        ),
        migrations.AddField(
            model_name='course',
            name='teachers',
            field=models.ManyToManyField(related_name='teachers_on_course', to='teachers.Teacher'),
        ),
    ]
