import random

from django.db import models

# Create your models here.
from faker import Faker

from groups.models import Group

from teachers.models import Teacher


class Course(models.Model):
    DIFFICULTY_CHOICES = [
        (0, 'Trivial'),
        (1, 'Easy'),
        (2, 'Medium'),
        (3, 'Expert'),
        (4, 'Hardcore'),
    ]
    name = models.CharField(max_length=200)
    difficulty = models.IntegerField(
        choices=DIFFICULTY_CHOICES,
        default=0)

    group = models.OneToOneField(
        to=Group,
        on_delete=models.SET_NULL,
        null=True,
        related_name='courses'
    )

    teachers = models.ManyToManyField(
        to=Teacher,
        related_name='teachers_on_course'
    )

    @classmethod
    def generate_courses(cls, count):
        faker = Faker()
        for _ in range(count):
            course_object = cls(
                name=faker.company(),
                difficulty=random.randint(0, 4)
            )
            course_object.save()
