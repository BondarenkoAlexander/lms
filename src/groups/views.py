from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404  # noqa
# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from groups.forms import GroupBaseForm, GroupCreateForm, GroupFilter, GroupUpdateForm
from groups.models import Group


class GroupListView(LoginRequiredMixin, ListView):
    paginate_by = 1
    model = Group
    template_name = 'groups-list.html'
    context_object_name = 'groups'

    def get_filter(self):
        return GroupFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()

        return context


class GroupCreateView(LoginRequiredMixin, CreateView):
    model = Group
    form_class = GroupCreateForm
    template_name = 'groups-create.html'
    success_url = reverse_lazy('groups:list')


class GroupUpdateView(LoginRequiredMixin, UpdateView):
    model = Group
    form_class = GroupUpdateForm
    success_url = reverse_lazy('groups:list')
    template_name = 'groups-update.html'
    pk_url_kwarg = 'group_id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['students'] = self.get_object().students.select_related('group', 'headed_group').all()
        context['teachers'] = self.get_object().teachers.all()
        return context


class GroupDeleteView(LoginRequiredMixin, DeleteView):
    model = Group
    form_class = GroupBaseForm
    template_name = 'groups-delete.html'
    success_url = reverse_lazy('groups:list')
    pk_url_kwarg = 'group_id'
