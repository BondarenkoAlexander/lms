from django.core.exceptions import ValidationError
from django.forms import ModelForm

import django_filters

from students.models import Student


class StudentFilter(django_filters.FilterSet):
    class Meta:
        model = Student
        # fields = ['first_name', 'last_name', 'age']
        fields = {
            'age': ['lt', 'gt'],
            'first_name': ['exact', 'icontains'],
            'last_name': ['exact', 'startswith'],
        }


class StudentBaseForm(ModelForm):
    class Meta:
        model = Student
        # fields = ['first_name', 'last_name', 'age']
        fields = '__all__'

    def clean(self):
        result = super().clean()
        enroll_date = self.cleaned_data['enroll_date']
        graduate_date = self.cleaned_data['graduate_date']
        if enroll_date > graduate_date:
            raise ValidationError('Enroll date couldn\'t be after graduate date')  # noqa

        return result


class StudentCreateForm(StudentBaseForm):
    pass


class StudentUpdateForm(StudentBaseForm):
    class Meta(StudentBaseForm.Meta):
        model = Student
        # fields = ['first_name', 'last_name', 'age']
        # fields = '__all__'
        exclude = ['age']
