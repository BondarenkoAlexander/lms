from classrooms.models import Classroom


from django.forms import ModelForm

import django_filters


class ClassroomFilter(django_filters.FilterSet):
    class Meta:
        model = Classroom
        fields = {
            'name': ['exact', 'icontains']
        }


class ClassroomBaseForm(ModelForm):
    class Meta:
        model = Classroom
        fields = '__all__'


class ClassroomUpdateForm(ClassroomBaseForm):
    pass


class ClassroomDeleteForm(ClassroomBaseForm):
    pass
